const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: './src/main/index.js',
  module: {
    rules: [
      // enable ESLint for all .js sources
      {
        test: /\.js$/,
        enforce: 'pre',
        exclude: /node_modules/,
        use: ['eslint-loader'],
      },
      // transpile all .js files from es6 to es5
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.jsx$/,
        enforce: 'pre',
        exclude: /node_modules/,
        use: ['eslint-loader'],
      },
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'classnames-loader',
          },
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: true,
              importLoaders: 2,
              localIdentName: '[name]__[local]___[hash:base64:5]',
            },
          },
          {
            loader: 'sass-loader',
          },
        ],
      },
      {
        test: /\.gif/,
        exclude: /node_modules/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'image/gif',
          },
        },
      },
      {
        test: /\.jpg/,
        exclude: /node_modules/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 100000,
            mimetype: 'image/jpg',
          },
        },
      },
      {
        test: /\.png/,
        exclude: /node_modules/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 100000,
            mimetype: 'image/png',
          },
        },
      },
      {
        test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
        loader: 'url-loader',
      },
      // load json
      {
        test: /\.json$/,
        exclude: /node_modules/,
        use: ['json-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  output: {
    path: __dirname + '/target',
    filename: 'index.js',
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({ template: 'src/main/index.html' })
  ],
  devServer: {
    port: 3000,
    hot: true,
    inline: true,
    historyApiFallback: true,
  },
};
