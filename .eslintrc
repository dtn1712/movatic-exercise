{
  // Extend existing configuration
  // from ESlint and eslint-plugin-react defaults.
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  // Enable ES6 support. If you want to use custom Babel features,
  // you will need to enable a custom parser as described in a section below.
  "parser": "babel-eslint",
  "env": {
    "es6": true,
    "browser": true,
    "node": true
  },
  // Enable custom plugin known as eslint-plugin-react
  "plugins": [
    "react"
  ],
  "parserOptions": {
    "sourceType": "module"
  },

  "rules": {
    "prop-types": ["off"],
    "linebreak-style": 0,
    "no-unused-vars": 0,
    "max-len": ["off"],
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    "object-curly-newline": [0, {
      "ObjectExpression": "never",
      "ObjectPattern": { "multiline": true },
      "ImportDeclaration": "never",
      "ExportDeclaration": { "multiline": true, "minProperties": 3 }
    }],
    "no-param-reassign": "off",
    "no-restricted-syntax": [
      "error",
      {
        "selector": "CallExpression[callee.object.name='console'][callee.property.name!=/^(log|warn|error|info|trace)$/]",
        "message": "Unexpected property on console object was called"
      }
    ],
    "react/forbid-prop-types": 0,
    "react/prefer-stateless-function": 0,
    "jsx-a11y/label-has-associated-control": 0,
    "jsx-a11y/label-has-for": 0,
    "comma-dangle": 0,
    "arrow-body-style": 0,
    "class-methods-use-this": 0,
    "no-undef": 0,
    "indent": 0,
    "arrow-parens": 0,
    "space-before-function-paren": 0,
    "prefer-arrow-callback": 0,
    "padded-blocks": 0,
    "func-names": 0,
    "prefer-const": 0,
    "no-plusplus": 0,
    "prefer-template": 0,
    "import/no-extraneous-dependencies": 0,
    "react/no-unused-state": 0,

    // enforces the use of === and !==
    eqeqeq: ["error", "always"],
    no-var: 'error',
    // Disable `no-console` rule
    "no-console": 0,
    // Give a warning if identifiers contain underscores
    "no-underscore-dangle": 1,
    "no-useless-escape": 0,
    // Default to single quotes and raise an error if something else is used
    "quotes": [ 2, "single" ],
    // Enforce max cyclomatic code complexity to be 22
    "complexity": [ 2, { max: 22 } ],
    // Disable requirement to defining all props types
    "react/prop-types": 0,
    // Tweak rule for unknown properties in elements because we use babel-plugin-react-html-attrs
    "react/no-unknown-property": [ 2, { ignore: [ "class", "for" ] } ],
    // Disable deprecated lifecycle method
    "react/no-deprecated": 0,
    "react/jsx-no-target-blank": 0
  }
}
