import { get } from './resource';

export default class StationResource {

  static getNYCStation() {
    return get('https://feeds.citibikenyc.com/stations/stations.json')({});
  }
}
