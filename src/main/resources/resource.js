import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import base64 from 'react-native-base64';

const superagent = superagentPromise(_superagent, global.Promise);

const rxOne = /^[\],:{}\s]*$/;
const rxTwo = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
const rxThree = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
const rxFour = /(?:^|:|,)(?:\s*\[)+/g;

const isJSON = input => input.length && rxOne.test(input.replace(rxTwo, '@').replace(rxThree, ']').replace(rxFour, ''));

const fillParams = (url, params) => {
  let queryParams = [];
  for (let key of Object.keys(params)) {
    if (url.indexOf(':' + key) !== -1) {
      url = url.replace(':' + key, encodeURIComponent(params[key]));
    } else {
      queryParams.push(key);
    }
  }
  if (queryParams.length > 0) {
    for (let i = 0; i < queryParams.length; i++) {
      const key = queryParams[i];
      const add = i === 0 ? '?' : '&';
      url += add + key + '=' + encodeURIComponent(params[key]);
    }
  }
  return url;
};

export const parseJSON = response => {
  return response.text().then(text => {
    return isJSON(text) ? JSON.parse(text) : null;
  });
};

export const get = url => {
  return function(params) {
    return new Promise(function(resolve, reject) {
      let errorFlag = false;
      fetch(fillParams(url, params), {
          method: 'GET',
        })
        .then(response => {
          if (response.status >= 400 && response.status < 600) {
            errorFlag = true;
          }
          return parseJSON(response);
        })
        .then(response => {
          if (errorFlag) {
            reject(response);
          } else {
            resolve(response);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
  };
};
