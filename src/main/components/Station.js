import React from 'react';
import { Table, Input, Button, Icon } from 'antd';
import Highlighter from 'react-highlight-words';

import classNames from 'classnames'
import classBind from 'classnames/bind'

import StationResource from '../resources/Station';

import styles from './Station.scss';

const cx = classBind.bind(styles)

class Station extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      stations: null,
      currentStation: null,
      dataSource: null,
      isLoading: true,
      searchText: '',
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    StationResource.getNYCStation().then(res => {
      if (Object.prototype.hasOwnProperty.call(res, 'stationBeanList')) {
        this.setState({
          stations: res.stationBeanList,
          isLoading: false
        }, () => {
          this.setTableData(this.state.stations);
        });
      }
    });
  }


  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };


  setTableData(stations) {
    const dataSource = stations.map(station => {
      return {
        key: station.id,
        id: station.id,
        stationName: station.stationName,
        availableDocks: station.availableDocks,
        totalDocks: station.totalDocks,
        latitude: station.latitude,
        longitude: station.longitude,
        statusValue: station.statusValue,
        statusKey: station.statusKey,
        availableBikes: station.availableBikes,
        stAddress1: station.stAddress1,
        stAddress2: station.stAddress2,
        city: station.city,
        postalCode: station.postalCode,
        location: station.location,
        altitude: station.altitude,
        testStation: station.testStation,
        lastCommunicationTime: station.lastCommunicationTime,
        landMark: station.landMark
      }
    })

    this.setState({
      dataSource
    })
  }

  goBackToListStation() {
    this.setState({
      currentStation: null
    })
  }

  getColumns(){
    return [
      {
        title: 'Id',
        dataIndex: 'id',
        key: 'id',
        sorter: (a, b) => a.id - b.id,
      },
      {
        title: 'Station Name',
        dataIndex: 'stationName',
        key: 'stationName',
        sorter: (a, b) => a.stationName.localeCompare(b.stationName),
        ...this.getColumnSearchProps('stationName'),
      },
      {
        title: 'Status',
        dataIndex: 'statusValue',
        key: 'statusValue',
        sorter: (a, b) => a.statusValue.localeCompare(b.statusValue),
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => {
          return (
            <Button type="primary" onClick={(e) => {
               e.stopPropagation();
               this.onClick(record);
              }}>
             View Detail
           </Button>
          )
        }
      },
    ]
  }

  onClick(station) {
    this.setState({
      currentStation: station
    })
  }

  render() {

    const { isLoading, currentPage, currentStation, dataSource } = this.state;

    const columns = this.getColumns();

    return (
      <div className={cx('home-page')}>
        <div className={cx('title')}>
          <div class='container text-center'>
            <h4>Newyork Bike Stations</h4>
          </div>
        </div>
        <div className={cx('content')}>
          <div className='container'>
            {currentStation === null && <Table
              loading={isLoading}
              dataSource={dataSource}
              columns={columns}
              pagination={{
                current: currentPage,
                onChange: (page, pageSize) => { this.setState({currentPage: page}) },
                pageSize: 20,
              }}
            />}
            { currentStation && (
              <div class='container'>
                <Button className={cx('button')} type="primary" onClick={(e) => {
                   e.stopPropagation();
                   this.goBackToListStation();
                  }}>
                 Go Back
               </Button>
                <table class="table">
                  <tbody>
                    <tr>
                      <td className={cx('table-cell')}>Station ID:</td>
                      <td className={cx('table-cell')}>{currentStation.id}</td>
                    </tr>
                    <tr>
                      <td className={cx('table-cell')}>Station Name:</td>
                      <td className={cx('table-cell')}>{currentStation.stationName}</td>
                    </tr>
                    <tr>
                      <td className={cx('table-cell')}>Available Docks:</td>
                      <td className={cx('table-cell')}>{currentStation.availableDocks}</td>
                    </tr>
                    <tr>
                      <td className={cx('table-cell')}>Total Docks:</td>
                      <td className={cx('table-cell')}>{currentStation.totalDocks}</td>
                    </tr>
                    <tr>
                      <td className={cx('table-cell')}>Latitude:</td>
                      <td className={cx('table-cell')}>{currentStation.latitude}</td>
                    </tr>
                    <tr>
                      <td className={cx('table-cell')}>Longitude:</td>
                      <td className={cx('table-cell')}>{currentStation.longitude}</td>
                    </tr>
                    <tr>
                      <td className={cx('table-cell')}>Status:</td>
                      <td className={cx('table-cell')}>{currentStation.statusValue}</td>
                    </tr>
                    <tr>
                      <td className={cx('table-cell')}>Available Bikes:</td>
                      <td className={cx('table-cell')}>{currentStation.availableBikes}</td>
                    </tr>
                    <tr>
                      <td className={cx('table-cell')}>Last Communication Time:</td>
                      <td className={cx('table-cell')}>{currentStation.lastCommunicationTime}</td>
                    </tr>
                    <tr>
                      <td className={cx('table-cell')}>Landmark:</td>
                      <td className={cx('table-cell')}>{currentStation.landMark}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            )}

          </div>
        </div>

      </div>
    );
  }
}

export default Station;
