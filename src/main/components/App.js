/* eslint-env browser */
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import classNames from 'classnames/bind';
import css from './App.scss';

import Station from './Station';

const cx = classNames.bind(css);

const Router = props => {
  let { headerHeight } = props;
  return (
    <main
      style={{ height: 'calc(100% - ' + headerHeight + 'px' }}
      id="mainContainer"
    >
      <div>
        <Switch>
          <Route path="/" component={Station} />
        </Switch>
      </div>
    </main>
  );
};

class App extends React.Component {
  render() {
    return (
      <div ref={(el) => { this.el = el; }} className={cx('layout')}>
        <Router />
      </div>
    );
  }
}

export default App;
